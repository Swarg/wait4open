--
require 'busted.runner' ()
local assert = require('luassert')

local bin_dir = './build/'
local prog_name = 'wait4open'


-- helper to run system command
local function run(bin, args)
  local cmd = bin or (bin_dir .. prog_name)

  local type_a = type(args)
  if type_a == 'table' then
    for _, arg in ipairs(args) do
      cmd = cmd .. ' ' .. arg
    end
  elseif type_a == 'string' then
    cmd = cmd .. ' ' .. args
  end
  cmd = cmd .. ' 2>&1' -- redirect stderr to stdout

  local h, err = io.popen(cmd)
  if err then
    return false, err
  elseif type(h) == 'string' then
    return false, h
    --
  elseif h then
    local res = h:read('*a')

    h:close()
    return true, res
  end
end

--
--
local USAGE =
    "Usage: ./build/wait4open " ..
    "[-v] [-q] [-d] [-t <timeout_sec>] [-s <interval_sec>] " ..
    "[<name>=]<host>:<port>\n" ..
    "-q quite (by default output is verbose)\n" ..
    "-d dry-run\n";

describe("wait4open", function()
  --
  it("compile", function()
    local st, res = run('make', 'wait4open')
    assert.same(true, st)
    assert.is_string(res)
    print(res)
  end)

  --
  it("parse_args version", function()
    local st, res = run(nil, '-v')
    assert.same(true, st)
    assert.is_string(res)
    assert.same("wait4open version 0.1.0\n", res)
  end)

  --
  it("parse_args help", function()
    local st, res = run(nil, '-h')
    assert.same(true, st)
    assert.is_string(res)
    local exp = USAGE
    assert.same(exp, res)
  end)

  --
  it("parse_args options t s and dry_run", function()
    local st, res = run(nil, '-t 4 -s 2 -d not_exist_host:54321')
    assert.same(true, st)
    assert.is_string(res)
    local exp =
        "lookup: not_exist_host:54321:  Name or service not known\n"..
        "Not found valid host:port in arguments\n" .. USAGE
    assert.same(exp, res)
  end)

  --
  it("parse_args dry_run host:port", function()
    local st, res = run(nil, '-d google.com:443')
    assert.same(true, st)
    assert.is_string(res)
    local exp =
        "%[DRY%-RUN%] Just show what will be done without the work itself\n" ..
        "timeout seconds: 0\n" ..
        "sleep interval : 1\n" ..
        "name: \n" ..
        "host: google.com\n" ..
        "port: 443\n" ..
        "addr: IP: .* Port: 443\n" -- parsed to struct addrinfo
    assert.match(exp, res)
  end)

  --
  it("parse_args dry_run name=host:port", function()
    local st, res = run(nil, '-d Google=google.com:443')
    assert.same(true, st)
    assert.is_string(res)
    local exp =
        "%[DRY%-RUN%] Just show what will be done without the work itself\n" ..
        "timeout seconds: 0\n" ..
        "sleep interval : 1\n" ..
        "name: Google\n" ..
        "host: google.com\n" ..
        "port: 443\n" ..
        "addr: IP: .* Port: 443\n" .. -- parsed to struct addrinfo
        "addr: IP: .* Port: 443\n"   -- linked list addr->next_ai
    assert.match(exp, res)
  end)

  it("parse_args dry_run name=host", function()
    local _, res = run(nil, '-d Google=google.com')
    assert.is_string(res)
    local exp = "Invailid argument (missing port): Google=google.com\n" .. USAGE
    assert.same(exp, res)
  end)

  --
  it("parse_args dry_run name=host", function()
    local _, res = run(nil, '-d x=y:z')
    assert.is_string(res)
    local exp =
        "lookup: y:z:  Servname not supported for ai_socktype\n" ..
        "Not found valid host:port in arguments\n" ..
        USAGE
    assert.same(exp, res)
  end)

  it("parse_args dry_run name=host", function()
    local _, res = run(nil, '-d =:')
    assert.is_string(res)
    local exp =
        "lookup: ::  Name or service not known\n" ..
        "Not found valid host:port in arguments\n" ..
        USAGE
    assert.same(exp, res)
  end)

  -- slow tests -- use internet connection

  --
  it("parse_args connect success", function()
    local st, res = run(nil, '-t 2 google.com:443')
    assert.same(true, st)
    assert.is_string(res)
    local exp = "Connecting google.com %(IP: .* Port: 443%) ... %[SUCCESS%].\n"
    assert.match(exp, res)
  end)

  --
  it("parse_args connect success", function()
    local st, res = run(nil, '-t 2 Google=google.com:443')
    assert.same(true, st)
    assert.is_string(res)
    local exp = "Connecting Google %(IP: .* Port: 443%) ... %[SUCCESS%].\n"
    assert.match(exp, res)
  end)

  --
  it("parse_args connect fail", function()
    local st, res = run(nil, '-t 1 localhost:65530')
    assert.same(true, st)
    assert.is_string(res)
    local exp =
        "Connecting localhost %(IP: .* Port: 65530%) ... %[FAILED%].\n" ..
        "Connecting localhost %(IP: .* Port: 65530%) ... %[FAILED%].\n" ..
        "Exit by timeout %[1%].\n"
    assert.match(exp, res)
  end)

  --
  it("parse_args connect fail", function()
    local st, res = run(nil, '-t 1 LH=localhost:65530')
    assert.same(true, st)
    assert.is_string(res)
    local exp =
        "Connecting LH %(IP: .* Port: 65530%) ... %[FAILED%].\n" ..
        "Connecting LH %(IP: .* Port: 65530%) ... %[FAILED%].\n" ..
        "Exit by timeout %[1%].\n"
    assert.match(exp, res)
  end)
end)
