# wait4open

The task of this program is to wait for the specified port to open.
You can specify the host address (name) and port which should be opened and
you can also set a waiting timeout(`-t`) and host check frequency(`-s`).

This program is written to work from inside lightweight alpine docker containers.

The main reason for writing this code in pure C is the desire for minimal
dependencies. It only requires the C library to work, and does not require
packages such as `bash` and `coreutils` and so on.

As intended, this program should be part of an docker image whose purpose is
short-term launches, for example, to execute console commands. (like php-cli)

## How to build locally

```sh
git clone https://gitlab.com/Swarg/wait4open
cd wait4open
make wait4open
./build/wait4open -h
```


## How to install locally
it will be installed into /usr/local/bin/wait4open

```sh
git clone https://gitlab.com/Swarg/wait4open
cd wait4open
make install
wait4open -h
```

## Command to build from source
from the project root:

```sh
gcc -pedantic -ansi -O2 ./src/main.c -o wait4open
```

## How to build and install inside the Dockerfile:

```Dockerfile
FROM alpine
COPY ./src/main.c /tmp/w4o.c

RUN apk update && apk add --no-cache --virtual .build-deps \
  g++ \

  && gcc -pedantic -ansi -O2 /tmp/w4o.c -o /usr/local/bin/wait4open \
  && chmod +x /usr/local/bin/wait4open \

  # .. build another deps for this cli-image from sources

  && apk del -f .build-deps
# ...
```

An example of a working dockerfile is provided at the root of the project,
so you can check the functionality of the program via:

```sh
git clone https://gitlab.com/Swarg/wait4open
cd wait4open
docker build -t img_w4o .
docker run --rm img_w4o wait4open -h
docker run --rm img_w4o wait4open -t 4 google.com:443
docker rmi img_w4o
```

```sh
docker run --rm img_w4o wait4open -t 30 api-postgres:5432
Connecting api-postgres (IP: xx.xx.xx.xx Port: 5432) ... [SUCCESS].
```

## Usage

```
wait4open [-v] [-q] [-d] [-t <N>] [-s <N>] [<name>=]<host>:<port>
```

- `-t` -- timeout seconds. Default is 0 - forever waiting until port opened
- `-s` -- sleep interval in secods. Default is 1 (second)
- `-q` -- quite (by default output is verbose and show ip addr of the host)
- `-d` -- dry-run only show state without actual work (lookups result of given host)

## Usage Examples

```sh
wait4open -t 30 api-postgres:5432

wait4open -t 1 postgres=localhost:54321
Connecting postgres (IP: ::1 Port: 54321) ... [SUCCESS].

wait4open -q -t 4 postgres=localhost:54322
Exit by timeout [4].

wait4open -t 30 127.0.0.1:80
Connecting localhost (IP: ::1 Port: 80) ... [SUCCESS].

wait4open -t 30 web=localhost:http
Connecting web (IP: ::1 Port: 80) ... [SUCCESS].
```

Supported `EnvVar` and `-d` (dry-run mode)

```sh
W4O_TIMEOUT=4 W4O_SLEEP=2 build/wait4open -d web=localhost:http

[DRY-RUN] Just show what will be done without the work itself
timeout seconds: 4
sleep interval : 2
name: web
host: localhost
port: http
addr: IP: ::1 Port: 80
addr: IP: 127.0.0.1 Port: 80
```

## Testing

Testing is done using [busted](https://lunarmodules.github.io/busted/),
which can be installed using [luarocks](https://luarocks.org/) for [Lua](https://www.lua.org/).

to run tests from the project root
```sh
busted
```
