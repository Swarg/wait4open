PROG_NAME=wait4open

SRCDIR=./src
BUILD_DIR=./build

CC=gcc
LD=gcc
CFLAGS=-Wall -g -pedantic -ansi -O2

# Create list of all sources files
CFILES=$(foreach D,$(SRCDIR),$(wildcard $(D)/*.c))
# Map sources names to .o-object and .d-dep-files (put .o and .d in build dir)
# [src/1.c src/2.c] -> [build/1.o build/2.o]
OBJECTS=$(foreach D,$(CFILES:.c=.o),$(BUILD_DIR)/$(notdir $(D)))

.PHONY: $(PROG_NAME) dirs install

dirs: $(BUILD_DIR)

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(PROG_NAME): $(OBJECTS) | dirs
	$(LD) -o $(BUILD_DIR)/$@ $^

# Compile
$(BUILD_DIR)/%.o: $(SRCDIR)/%.c | dirs
	$(CC) -c $(CFLAGS) -o $@ $<


install:
	mkdir -p $(PREFIX)/usr/local/bin
	install -m 0755 $(BUILD_DIR)/wait4open $(PREFIX)/usr/local/bin/wait4open

check:
	sh -c './build/wait4open -t 1 local=127.0.0.1:65535 ; st=$$? ; [ $$st -eq 2 ] || [ $$st -eq 0 ]; echo $$st'
