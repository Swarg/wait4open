# This Dockerfile is just an example of
# how you can embed this program into your Dockerfile with some cli-stuff

FROM alpine

COPY ./src/main.c /tmp/w4o.c

# instead the "g++" package it can be the "build-base" package

RUN apk update && apk add --no-cache --virtual .build-deps \
    g++ \

    && gcc -pedantic -ansi -O2 /tmp/w4o.c -o /usr/local/bin/wait4open \
    && chmod +x /usr/local/bin/wait4open \

    # here we can build an another cli-stuff from a sources

    && apk del -f .build-deps

# ...


